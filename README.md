# Internship Interview Questions
*Please include explanation along with your answers.*

## 1. Please describe yourself using JSON (include your internship start/end date).

```
{
    "name": "Muhammad Fathy Rashad",
    "internship": {
        "start_date": "14 September 2020",
        "end_date": "18 December 2020"
    },
    "portfolio": "www.mfrashad.com",
    "linkedin": "https://www.linkedin.com/in/mfathyrashad",
    "education": "Universiti Teknologi PETRONAS | Computer Engineering (Hons) | CGPA 3.74/4 | 2017-2021",
    "experience":[
        "Software Engineer Intern | Fave",
        "Freelance Developer",
        "Mobile Developer (Part-Time) | Universiti Teknologi PETRONAS (UTP)",
        "Research Assistant | Universiti Teknologi PETRONAS (UTP)",
        "Computer Vision and Deep Learning R&D Intern | ViTrox"
    ],
    "leadership":[
        "Founder & President | UTP App Development Club",
        "Tech Lead | DSC UTP by Google Developers"
    ],
    "certification": ["Microsoft Certified: Azure AI Engineer Associate"],
    "awards":[
        "Best Open Source Project on GitHub’s Award out of 292 participants globally in the Local Hack Day: Share competition",
        "First Runner Up nationwide in iUM Hackathon 2020.",
        "2nd Place nationwide in DSC Kita Hack 2020 by Google Developers.",
        "Chairman’s Award out of 160 teams in the 42nd Science & Engineering Exhibition.",
        "Ranked 9 nationwide in varsity APU Battle of Hackers 2019 CTF (Capture The Flag) cybersecurity competition."
    ],
    "skills":[
        "Python", "JavaScript", "TypeScript", "React", "Angular", "React Native","Firebase", "Ruby on Rails", "Flutter", "Bash", "Git", "AWS", "GCP", "Docker", "Kubernetes"
    ]
}
```

## 2. Tell us about a newer (less than five years old) web technology you like and why?

I learned Flutter not long ago, a framework that enable you to build for web, android and ios from a single codebase. I learned it when I needed to do a workshop for other students and also wrote a tutorial oof it on codelab.

As a user of React Native which have a similar use with Flutter, I found flutter is a lot more developer friendly. There are many small features that make the experience in developing in Flutter much more enjoyable. The fact that they use a strongly typed language, Dart, also make developing and debuggingg easier.

Additionally, the many built-in components in Flutter make developing a beautiful app in Flutter much faster compared to React Native, in my experience.

However, I have not explored Flutter deeply, and have not explored how it will fare in more complex codebases.

## 3. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that (other than using ArrayList)?

Use an array with some initial size. When array is full, create a new array with increased size (usually double the size) and copy the previous array content to the new array.

Alternatively, we can use a linked list where it has faster insertion/deletion time, but it will have a slower access time.

## 4. Explain this block of code in Big-O notation.
    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```
Big-O notation describe the relationship between the performance (either time or space) and the size of the input. The code above will have time complexity of O(n^2), where n is the input, which is varible `size` in this case.

The code above will have n*n loops. As n increase to extremely large values, the execution time will largely comes from the executiom time for each loop, hence the execution time will directly proportional to square of the input size n^2. It is the same case with the space complexity.


## 5. One of our developers built a page to display market information for over 500 cryptocurrencies in a single page. After several days, our support team started receiving complaints from our users that they are not able to view the website and it takes too long to load. What do you think causes this issue and suggest at least 3 ideas to improve the situation. 

1. If the server previously is not scalable, we can scale the server vertically or use a load balancer and scale horizonally. Deploying on cloud that have autoscale feature is recommended.
2. If the cryptocurrency data is not time critical, we can cache the data and only update the data after certain period of time for the user, so user do not need to request new data every visit.
3. If data need to be real time, we should use proper communication channel. Use WebSocket or SSE (Server Sent Event) to get real time data instead of constantly polling data.
4. Split the display into several pages, instead of 500 on one page.
5. Compress any data served to the user (eg. Images, js, or css files )

## 6. In Javascript, What is a "closure"? How does JS Closure works?
When inner function have access to outer's variable/scope


## 7. In Javascript, what is the difference between var, let, and const. When should I use them?

var: global scoped or function/local scoped, can be updated
let: block scoped, can be updated
const: block scoped, constant/cannot be updated

---
# Simple Coding Assessment

Build a URL shortener service (refers to https://bitly.com)

requirement:
1. build a method/function where it takes in an URL (example www.coingecko.com), and the output is a shortened URL
2. the shortened URL must be unique.
3. build a method/function where it takes in the shortened URL in #1, and the output is the full URL.
4. you must be able to demo your code to us.
5. **(bonus point)** use Ruby On Rails and/or ReactJS as the language/framework.
6. **(bonus point)** host it on a website.

## URL Shortener

Unfortunately, I am quite busy with my current internship, contract work, and competition. Hence, I do not have time to create a new project. However, I did a similar URL shortener project in NodeJS 2 years ago. 

It can be accessed in : https://mfrashad-urlshortener.herokuapp.com

GitHub Repo: https://github.com/mfrashad/url-shortener

(current bug: url cannot have protocol (or slash), 'https://' or 'http://' )

---

I also did many other projects in React, React Native, and Ruby on Rails. My projects can be seen at my website or GitHub account.

Website: https://www.mfrashad.com/

GitHub: https://github.com/mfrashad

---
# Submission instruction

1. fork this repo
2. creates a Merge Request against the master branch (after completion)
3. grant repo access (collaborators) to jack@coingecko.com and tmlee@coingecko.com
